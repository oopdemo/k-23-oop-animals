//
// Created by sd on 05/10/2020.
//

#ifndef K_23_LEC_OOP2_CAT_H
#define K_23_LEC_OOP2_CAT_H

#include "animal.h"

class Cat : public Animal {
public:
    void voice();
};

#endif //K_23_LEC_OOP2_CAT_H
