//
// Created by sd on 05/10/2020.
//

#include <string>
#include "animal.h"

void Animal::eat() {
    weight += 1;
}

int Animal::getWeight() {
    return weight;
}

int Animal::getAge() {
    return age;
}

void Animal::setAge(int newAge) {
    if(newAge>0 && newAge < 100) {
        age = newAge;
    }
}
