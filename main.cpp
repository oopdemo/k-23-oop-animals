#include <string>
#include "elephant.h"
#include "cat.h"

// Абстракція, Інкапсуляція, ?Наследование
// Поліморфізм

void testElephant() {
    Elephant slo;
    //.......
}

int main() {
    int x;

    Elephant elya;
    Cat cat;

    testElephant();

    elya.name = "Elya";
    // elya.weight = 10000;
    elya.eat();
    elya.eat();
    elya.voice();

    cat.name = "Barsik";
    cat.voice();

    return 0;
}