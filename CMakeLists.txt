cmake_minimum_required(VERSION 3.17)
project(k_23_lec_oop2)

set(CMAKE_CXX_STANDARD 14)

add_executable(k_23_lec_oop2 main.cpp animal.cpp elephant.cpp cat.cpp)