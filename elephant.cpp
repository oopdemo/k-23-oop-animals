//
// Created by sd on 05/10/2020.
//

#include <string>
#include <iostream>

#include "elephant.h"

void Elephant::voice() {
    std::cout << "Tuu-Too! My name is " << name << "; I'm " << getAge() << " years old and my weight is " << weight << std::endl;
}
